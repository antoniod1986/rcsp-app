exports.config = {
  globalStyle: 'src/global/rcsp.css'
};

exports.devServer = {
  root: 'www',
  watchGlob: '**/**'
};
