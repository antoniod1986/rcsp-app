# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.1.1"></a>
## [0.1.1](https://github.com/ionic-team/stencil-app-starter/compare/v0.1.0...v0.1.1) (2018-07-01)



<a name="0.1.0"></a>
# 0.1.0 (2018-07-01)


### Bug Fixes

* euro sign + replaced . with , on amounts ([b3f9162](https://github.com/ionic-team/stencil-app-starter/commit/b3f9162))
* lint issues ([e8431c3](https://github.com/ionic-team/stencil-app-starter/commit/e8431c3))
* router issue solved importing module ([b9b105d](https://github.com/ionic-team/stencil-app-starter/commit/b9b105d))
* styling improvements ([dca47a5](https://github.com/ionic-team/stencil-app-starter/commit/dca47a5))


### Features

* added coverage script + .gitignore ([8647e6c](https://github.com/ionic-team/stencil-app-starter/commit/8647e6c))
* generator ([47cd269](https://github.com/ionic-team/stencil-app-starter/commit/47cd269))
* Parser singleton ([2839a67](https://github.com/ionic-team/stencil-app-starter/commit/2839a67))
* Reader singleton ([2986524](https://github.com/ionic-team/stencil-app-starter/commit/2986524))
* record component ([e372b0a](https://github.com/ionic-team/stencil-app-starter/commit/e372b0a))
* table component ([b0f4b8c](https://github.com/ionic-team/stencil-app-starter/commit/b0f4b8c))
* Transaction interface ([2671e64](https://github.com/ionic-team/stencil-app-starter/commit/2671e64))



<a name="0.0.2"></a>
## 0.0.2 (2018-06-24)


### Bug Fixes

* router issue solved importing module ([b9b105d](https://github.com/ionic-team/stencil-app-starter/commit/b9b105d))
