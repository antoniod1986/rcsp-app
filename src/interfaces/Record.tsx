interface RecordInterface {
  reference: string;
  description: string;
  error: string;
}
