interface TransactionInterface {
  reference: string;
  account: string;
  description: string;
  startBalance: number;
  mutation: number;
  endBalance: number;
}
