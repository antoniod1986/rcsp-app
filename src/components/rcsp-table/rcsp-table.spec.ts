import { TestWindow } from '@stencil/core/testing';
import { RcspTable } from './rcsp-table';

describe('app', () => {
  it('should build', () => {
    expect(new RcspTable()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLRcspTableElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      this.records = JSON.stringify([
        { reference: '1', description: 'D1', error: 'E1' },
        { reference: '2', description: 'D2', error: 'E2' },
        { reference: '3', description: 'D3', error: 'E3' }
      ]);
      this.records2 = JSON.stringify([
        { reference: '4', description: 'D4', error: 'E4' },
        { reference: '5', description: 'D5', error: 'E5' }
      ]);
    });

    it('creates the element', async () => {
      element = await testWindow.load({
        components: [RcspTable],
        html: `<rcsp-table records='${this.records}'></rcsp-table>`
      });
      expect(element).toBeTruthy();
    });

    it('renders headers', async () => {
      element = await testWindow.load({
        components: [RcspTable],
        html: `<rcsp-table records='${this.records}'></rcsp-table>`
      });
      const reference = element.querySelector('.header--reference');
      const description = element.querySelector('.header--description');
      const error = element.querySelector('.header--error');
      expect(reference.textContent).toBe('Reference');
      expect(description.textContent).toBe('Description');
      expect(error.textContent).toBe('Error');
    });

    it('prints all records provided', async () => {
      element = await testWindow.load({
        components: [RcspTable],
        html: `<rcsp-table records='${this.records}'></rcsp-table>`
      });
      const recordsRendered = element.querySelectorAll('rcsp-record');
      expect(recordsRendered.length).toBe(3);
    });

    it('shows a message in case of zero records', async () => {
      element = await testWindow.load({
        components: [RcspTable],
        html: `<rcsp-table records='${[]}'></rcsp-table>`
      });
      const noRecordsSpan = element.querySelector('.no-records');
      expect(noRecordsSpan).toBeTruthy();
      expect(noRecordsSpan.textContent).toBe('No records available. All transactions were validated.');
    });

    it('updates the report changing the input records', async () => {
      element = await testWindow.load({
        components: [RcspTable],
        html: `<rcsp-table records='${this.records}'></rcsp-table>`
      });
      let recordsRendered = element.querySelectorAll('rcsp-record');
      expect(recordsRendered.length).toBe(3);

      element.records = '[]';
      testWindow.flush();

      const noRecordsSpan = element.querySelector('.no-records');
      expect(noRecordsSpan).toBeTruthy();
      expect(noRecordsSpan.textContent).toBe('No records available. All transactions were validated.');

      element.records = this.records2;
      testWindow.flush();

      recordsRendered = element.querySelectorAll('rcsp-record');
      expect(recordsRendered.length).toBe(2);
    });
  });
});
