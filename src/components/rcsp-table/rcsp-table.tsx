import { Component, Prop } from '@stencil/core';

@Component({
  tag: 'rcsp-table',
  styleUrl: 'rcsp-table.css'
})

export class RcspTable {
  private reference = 'Reference';
  private description = 'Description';
  private error = 'Error';
  private noRecordsMessage = 'No records available. All transactions were validated.';
  private innerRecords: RecordInterface[];
  @Prop() records: string;

  render() {
    if (this.innerRecords.length) {
      return (
        <div class="table">
          <div class="headers">
            <div class="header header--reference">{this.reference}</div>
            <div class="header header--description">{this.description}</div>
            <div class="header header--error">{this.error}</div>
          </div>
          {this.innerRecords.map((record, index) =>
            <rcsp-record
              reference={record.reference}
              description={record.description}
              error={record.error}
              even={ (index % 2) ? true : false }
            ></rcsp-record>
          )}
        </div>
      );
    } else {
      return (<span class="no-records">{this.noRecordsMessage}</span>);
    }
  }

  componentWillLoad(): void {
    this.innerRecords = JSON.parse(this.records || '[]');
  }

  componentWillUpdate(): void {
    this.innerRecords = JSON.parse(this.records || '[]');
  }
}
