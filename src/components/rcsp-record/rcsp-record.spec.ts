import { TestWindow } from '@stencil/core/testing';
import { RcspRecord } from './rcsp-record';

describe('app', () => {
  it('should build', () => {
    expect(new RcspRecord()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLRcspRecordElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
    });

    it('creates the element', async () => {
      element = await testWindow.load({
        components: [RcspRecord],
        html: '<rcsp-record></rcsp-record>'
      });
      expect(element).toBeTruthy();
    });

    it('renders values based on props', async () => {
      element = await testWindow.load({
        components: [RcspRecord],
        html: `
          <rcsp-record
            reference="12345678"
            description="Description"
            error="Error"
          ></rcsp-record>`
      });
      const reference = element.querySelector('.slot--reference');
      const description = element.querySelector('.slot--description');
      const error = element.querySelector('.slot--error');
      expect(reference.textContent).toBe('12345678');
      expect(description.textContent).toBe('Description');
      expect(error.textContent).toBe('Error');
    });

    it('sets different background color for even record', async () => {
      element = await testWindow.load({
        components: [RcspRecord],
        html: `
          <container>
            <rcsp-record></rcsp-record>
            <rcsp-record even={true}></rcsp-record>
          </container>
        `
      });
      const records = element.querySelectorAll('.record');
      expect(records[0].classList.contains('record--even')).toBeFalsy();
      expect(records[1].classList.contains('record--even')).toBeTruthy();
    });
  });
});
