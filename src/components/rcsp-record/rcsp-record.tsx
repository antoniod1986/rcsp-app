import { Component, Prop } from '@stencil/core';

@Component({
  tag: 'rcsp-record',
  styleUrl: 'rcsp-record.css'
})

export class RcspRecord {
  @Prop() reference: string;
  @Prop() description: string;
  @Prop() error: string;
  @Prop() even: boolean;

  render() {
    let recordClassList = 'record';
    recordClassList += (this.even) ? ' record--even' : '';
    return (
      <div class={recordClassList}>
        <div class="slot slot--reference">{this.reference}</div>
        <div class="slot slot--description">{this.description}</div>
        <div class="slot slot--error">{this.error}</div>
      </div>
    );
  }
}
