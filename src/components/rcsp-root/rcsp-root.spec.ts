import { TestWindow } from '@stencil/core/testing';
import { RcspRoot } from './rcsp-root';

describe('rcsp-root', () => {
  it('should build', () => {
    expect(new RcspRoot()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLRcspRootElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [RcspRoot],
        html: '<rcsp-root></rcsp-root>'
      });
    });

    it('creates the element', () => {
      expect(element).toBeTruthy();
    });
  });
});
