import { Component } from '@stencil/core';
import '@stencil/router';

@Component({
  tag: 'rcsp-root',
  styleUrl: 'rcsp-root.css'
})
export class RcspRoot {
  render() {
    return (
      <div>
        <header>
          <h1>Customer Statement Processor</h1>
        </header>

        <main>
          <stencil-router>
            <stencil-route url="/" component="rcsp-generator" exact={true}>
            </stencil-route>
          </stencil-router>
        </main>
      </div>
    );
  }
}
