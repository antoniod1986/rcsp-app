import { TestWindow } from '@stencil/core/testing';
import { RcspGenerator } from './rcsp-generator';

describe('app', () => {
  it('should build', () => {
    expect(new RcspGenerator()).toBeTruthy();
  });

  describe('rendering', () => {
    let element: HTMLRcspGeneratorElement;
    let testWindow: TestWindow;
    beforeEach(async () => {
      testWindow = new TestWindow();
      element = await testWindow.load({
        components: [RcspGenerator],
        html: '<rcsp-generator></rcsp-generator>'
      });
    });

    it('creates the element', () => {
      expect(element).toBeTruthy();
    });
  });
});
