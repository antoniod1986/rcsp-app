import { Component, State } from '@stencil/core';
import { Reader } from '../../utils/Reader';
import { Generator } from '../../utils/Generator';

@Component({
  tag: 'rcsp-generator',
  styleUrl: 'rcsp-generator.css'
})
export class RcspGenerator {
  @State() files: any[] = [];
  @State() label = 'Choose your files';
  @State() records: RecordInterface[] = [];
  private accept = '.csv,.xml';

  render() {
    return (
      <div class="rcsp-generator">
        <div class="loader">
          <input
            type="file"
            name="file"
            id="file"
            accept={this.accept} multiple
            value={this.files} onInput={(event) => this.handleChange(event)}
            class="inputfile" />
          <label htmlFor="file">{this.label}</label>
        </div>
        <rcsp-table records={JSON.stringify(this.records)}></rcsp-table>
      </div>
    );
  }

  handleChange(event) {
    this.files = event.target.files;
    this.changeLabel();
    if (this.files.length) {
      Reader.instance.read(this.files)
        .then((data) => {
          this.records = Generator.instance.generate(data);
        })
        .catch((error) => {
          console.error(error);
        });
    } else {
      this.records = [];
    }
  }

  changeLabel() {
    const length = this.files.length;
    if (length === 0) {
      this.label = 'Choose your files';
    } else if (length === 1) {
      this.label = this.files[0].name;
    } else if (length > 1) {
      this.label = `${length} files selected`;
    }
  }
}
