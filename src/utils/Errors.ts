export class Errors {
  static get(id: string, ...args: string[]) {
    const errors: object = {
      ERR01: `Duplicate of the following transaction: ${args[0]}`,
      ERR02: `Expected balance should be ${args[0]}€ and not ${args[1]}€`,
    };
    return errors[id];
  }
}
