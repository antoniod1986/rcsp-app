import Parser from './Parser';

const _singleton = Symbol();

export class Reader {
  constructor(singletonToken) {
    if (_singleton !== singletonToken) {
      throw new Error('Cannot instantiate directly.');
    }
  }

  static get instance() {
    if (!this[_singleton]) {
      this[_singleton] = new Reader(_singleton);
    }

    return this[_singleton];
  }

  read(files: Blob[]) {
    const readerQueue: Promise<TransactionInterface[]>[] = [];

    return new Promise((resolve) => {
      for (let i = 0; i < files.length; i++) {
        readerQueue.push(this.readFile(files[i]));
      }
      Promise.all(readerQueue)
        .then(data => {
          resolve(data);
        })
        .catch(error => {
          console.error(error);
        });
    });
  }

  private readFile(file: Blob) {
    let promise: Promise<TransactionInterface[]>;
    const reader = new FileReader();

    reader.readAsText(file, 'UTF-8');
    promise = new Promise<TransactionInterface[]>((resolve) => {
      reader.onload = () => {
        resolve(Parser.instance.parse(reader.result, file.type));
      };
      reader.onerror = (e) => {
        console.error('error reading file', e);
        resolve([]);
      };
    });

    return promise;
  }
}
