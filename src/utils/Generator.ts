import { Errors } from './Errors';

const _singleton = Symbol();

export class Generator {
  private report: RecordInterface[];

  constructor(singletonToken) {
    if (_singleton !== singletonToken) {
      throw new Error('Cannot instantiate directly.');
    }
  }

  static get instance() {
    if (!this[_singleton]) {
      this[_singleton] = new Generator(_singleton);
    }

    return this[_singleton];
  }

  generate(transactionFiles: TransactionInterface[][]) {
    let transactions: TransactionInterface[] = [];
    this.report = [];

    transactionFiles.forEach((file) => {
      transactions = transactions.concat(file);
    });

    transactions.forEach((transaction, index, object) => {
      if (!this.isBalanceValid(transaction)) {
        object.splice(index, 1);
      } else if (!this.isUnique(transaction, index, object)) {
        object.splice(index, 1);
      }
    });

    return this.report;
  }

  private isUnique(transaction: TransactionInterface, index: number, transactions: TransactionInterface[]) {
    for (let i = 0; i < transactions.length; i++) {
      if (i === index) {
        continue;
      }
      if (transaction.reference === transactions[i].reference) {
        const record = {
          reference: transaction.reference,
          description: transaction.description,
          error: Errors.get('ERR01', transactions[i].description)
        };
        this.report.push(record);
        return false;
      }
    }
    return true;
  }

  private isBalanceValid(transaction: TransactionInterface) {
    const expectedBalance = transaction.startBalance + transaction.mutation;
    if (expectedBalance.toFixed(2) !== transaction.endBalance.toFixed(2)) {
      const record = {
        reference: transaction.reference,
        description: transaction.description,
        error: Errors.get('ERR02', expectedBalance.toFixed(2).replace('.', ','), transaction.endBalance.toFixed(2).replace('.', ','))
      };
      this.report.push(record);

      return false;
    }

    return true;
  }
}
