const _singleton = Symbol();

export default class Parser {
  constructor(singletonToken) {
    if (_singleton !== singletonToken) {
      throw new Error('Cannot instantiate directly.');
    }
  }

  static get instance() {
    if (!this[_singleton]) {
      this[_singleton] = new Parser(_singleton);
    }

    return this[_singleton];
  }

  parse(file: string, type: string) {
    let parsed: TransactionInterface[];

    switch (type) {
      case 'text/xml': {
        parsed = this.xmlParser(file);
        break;
      }
      case 'text/csv': {
        parsed = this.csvParser(file);
        break;
      }
      default: parsed = [];
    }

    return parsed;
  }

  private xmlParser(file: string) {
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(file, 'text/xml');
    const records = xmlDoc.querySelectorAll('record');
    const transactions: TransactionInterface[] = [];

    Array.from(records).forEach((record) => {
      try {
        const transaction: TransactionInterface = {
          reference: record.getAttribute('reference'),
          account: record.querySelector('accountNumber').textContent,
          description: record.querySelector('description').textContent,
          startBalance: parseFloat(record.querySelector('startBalance').textContent),
          mutation: parseFloat(record.querySelector('mutation').textContent),
          endBalance: parseFloat(record.querySelector('endBalance').textContent)
        };
        transactions.push(transaction);
      } catch (e) {
        console.error(e, record);
      }
    });

    return transactions;
  }

  private csvParser(file: string) {
    const transactions: TransactionInterface[] = [];
    let rows: string[] = [];

    try {
      rows = file.split(/\r\n\t|\n|\r\t/gm).splice(1);
    } catch (e) {
      console.error(e);
    }

    rows.forEach((row) => {
      if (row === '') {
        return;
      }
      try {
        const record: string[] = row.split(',');
        const transaction: TransactionInterface = {
          reference: record[0],
          account: record[1],
          description: record[2],
          startBalance: parseFloat(record[3]),
          mutation: parseFloat(record[4]),
          endBalance: parseFloat(record[5])
        };
        transactions.push(transaction);
      } catch (e) {
        console.error(e, row);
      }
    });

    return transactions;
  }
}
