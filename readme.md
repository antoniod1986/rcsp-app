# Customer Statement Processor

This app validates transaction files and in case of errors
generates a report with both reference and description of each of the failed records.

## Technologies used

This project was generated and developed using [Stencil JS](https://stenciljs.com/).
Test and test coverage uses [Jest](http://jestjs.io/) and code is linted using [tslint](https://palantir.github.io/tslint/) module.
Versioning over *GIT*, was made using [standard-version](https://www.npmjs.com/package/standard-version)

## How to run

Clone this repo:

```bash
git clone git@bitbucket.org:antoniod1986/rcsp-app.git
npm install
npm start
```

To view the build, start an HTTP server inside of the `/www` directory.
In the `assets` folder, you can find a *.csv* and a *.xml* sample files.

## ...and more

To check lint coverage (tslint) run:

```
npm run lint
```

To run the unit tests once, run:

```
npm test
```

To check the test coverage, run:

```
npm run coverage
```

## Issues with build

May be occur some issue starting the project due to ES6 support.
In that case, just run:

```
npm run dev --es5
```

In case the *npm start* runs fine but you get a JSON response page, just refresh the page.